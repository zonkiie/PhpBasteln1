<?php

/* 
 * Copyright (C) 2017 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once(__DIR__ . "/../classes/Init.php");
use Utils\FilesystemFunctions;

$pwd = realpath(".");
echo "Pwd:" . $pwd . PHP_EOL;

$Mountpoint = FilesystemFunctions::FindMountPoint($pwd);
echo "Mountpoint:" . $Mountpoint . PHP_EOL;

$device = FilesystemFunctions::getdev(".");
echo "Device:" . $device . PHP_EOL;

