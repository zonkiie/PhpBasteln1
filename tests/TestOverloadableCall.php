<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once '../classes/Init.php';

use \Utils\TraitOverloadableCall;

class c1
{
    public function __toString() {
        return "Klasse:" . get_class();
    }
}

/**
 * Description of TestOverloadableCall
 *
 * @author rainer
 */
class TestOverloadableCall {
    use TraitOverloadableCall;
    
    public function __construct() {
        #print_r(get_class_methods($this));
    }
    
    public function __call($name, $arguments) {
        return $this->CallOverload($name, $arguments);
    }
    
    public function handle_integer($i)
    {
        echo __FUNCTION__ . ":" . $i .PHP_EOL;
    }
    public function handle_string($i)
    {
        echo __FUNCTION__ . ":" . $i .PHP_EOL;
    }
    
    public function handle_c1(c1 $i)
    {
        echo __FUNCTION__ . ":" . $i . PHP_EOL;
    }
    
}

$obj = new TestOverloadableCall();
$obj->handle(1);
$obj->handle("Hallo");
$obj->handle(new c1());

