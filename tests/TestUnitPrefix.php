<?php

/* 
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once '../classes/Init.php';

use Utils\UnitUtils;

$num = sprintf("%.3f", sqrt(2)) * pow(1024, 3);

$normalized = UnitUtils::CalculateToUnitPrefix($num);
echo "Num:" . $num . PHP_EOL;
print_r($normalized);

$num2 = UnitUtils::CalculateFromUnitPrefix($normalized['Value'], $normalized['UnitPrefix']);
echo "Num2:" . $num2 . PHP_EOL;
