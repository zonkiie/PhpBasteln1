<?php

/* 
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once '../classes/Init.php';

use Utils\ClassFinder;

$cf = ClassFinder::newInstance()->SetBaseDir(__DIR__ . "/../classes")->SetRecursive(TRUE);
$classes = $cf->GetClassesInFile(__DIR__ . "/../classes/Utils/ObjectFeeder.php");
echo "Classes in File " . print_r($classes, true) . PHP_EOL;

$dirclasses = array();
$cf->FindAllClassesInDir($dirclasses, __DIR__ . "/../classes");
echo "Classes in class Dir:" . print_r($dirclasses, true) . PHP_EOL;

$classes = $cf->FindAllClassesInNamespace("\\Utils");
print_r($classes);

$classes = $cf->FindAllClassesImplementingInterface("\\MyNamespace\\Interface1", "\\MyNamespace");
echo "Classes implementing Interface1: " . print_r($classes, true) . PHP_EOL;

$classes = $cf->FindAllClassesExtendBaseClass("\\MyNamespace\\ImplementingClass", "\\MyNamespace");
echo "Classes extending ImplementingClass: " . print_r($classes, true) . PHP_EOL;
