<?php

/* 
 * Copyright (C) 2017 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once(__DIR__ . "/../classes/Init.php");
use Utils\FunctionUtils;

function func($arg1, $arg2 = "Jane")
{
    return $arg1 . "," . $arg2;
}

class tc1
{
    public static function func1($Arg1, $Arg2 = "HH")
    {
        return $Arg1 . " " . $Arg2;
    }
}

$func2 = function($a1, $a2)
{
   return "a1:" . $a1 . ", a2:" . $a2;  
};

$ret = FunctionUtils::call_user_function_named_param('func', array("arg1"=>"Hello", "arg2"=>"Joe"));
echo "Call Return:" . print_r($ret, true) . PHP_EOL;
$ret = FunctionUtils::call_user_function_named_param(array(new tc1(), 'func1'), array("Arg1"=>"Hello", "Arg2"=>"Joe"));
echo "Call2 Return:" . print_r($ret, true) . PHP_EOL;
$ret = FunctionUtils::call_user_function_named_param($func2, array("a1"=>"Hello", "a2"=>"Joe"));
echo "Call3 Return:" . print_r($ret, true) . PHP_EOL;
