<?php

/* 
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once '../classes/Init.php';

/*
 * Example: Railway lines between following cities. But we mesaure only air distance.
 * 0 = Nürnberg
 * 1 = Regensburg
 * 2 = Ingolstadt
 * 3 = Landshut
 * 4 = München
 * 
 * Distance
 * F/T  0   1   2   3   4
 *  0   0  90  82   -   -  
 *  1  90   0  57  52   - 
 *  2  82  57   0   -  68 
 *  3   -  52   -   0  62 
 *  4   -   -  68  62   0 

*/
$Graph = array(
    0=>array(0=>0,              1=>90,          2=>82,          3=>PHP_INT_MAX, 4=>PHP_INT_MAX),
    1=>array(0=>90,             1=>0,           2=>57,          3=>52,          4=>PHP_INT_MAX),
    2=>array(0=>82,             1=>57,          2=>0,           3=>PHP_INT_MAX, 4=>68),
    3=>array(0=>PHP_INT_MAX,    1=>52,          2=>PHP_INT_MAX, 3=>0,           4=>62),
    4=>array(0=>PHP_INT_MAX,    1=>PHP_INT_MAX, 2=>68,          3=>62,          4=>0)
);

$Pathfinder = new \Algorithms\Graph\ShortestPath\DijkstraShortestPath($Graph);
$Pathfinder->setStartingPoint(0)->setTargetPoint(4);
$Path = $Pathfinder->FindShortestPath();
echo "Result:" . print_r($Path, true) . PHP_EOL;
