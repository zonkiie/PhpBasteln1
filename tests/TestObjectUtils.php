<?php

/* 
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once '../classes/Init.php';

use Utils\ObjectUtils;

class TestClass1
{
    public $pub_var = "public_value";
    private $priv_var = "private_value";
    protected  $prot_var = "protected_value";
    var $var_value = "var_value";
    
    /** @ignore */
    public $annotated_value = "annotated public value";
}

$obj = new TestClass1();

print_r($obj);
echo "emptying @ignore-anntoated fields\r\n";
ObjectUtils::SetValueByAnnotation($obj, "@ignore", null);
print_r($obj);
echo "emptying private and protected fields\r\n";
ObjectUtils::SetValueByAccessModifier($obj, ReflectionProperty::IS_PRIVATE|ReflectionProperty::IS_PROTECTED, null);
print_r($obj);
