<?php

/*
 * Copyright (C) 2018 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace DBUtils;

class PDOStructureCheck {
    /** pdo connection */
    private $pdo;

    public function SetPDO($pdo) {
        $this->pdo = $pdo;
        return $this;
    }

    function GetColumns($TableName) {
        $result = array();
        switch ($this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME)) {
            case "sqlite":
                $query = "PRAGMA table_info('" . $TableName . "')";
                foreach ($this->pdo->query($query) as $row)
                    $result[] = $row['name'];
                break;
            case "mysql":
                $query = "SHOW COLUMNS FROM '$TableName'";
                foreach ($this->pdo->query($query) as $row)
                    $result[] = $row['Field'];
                break;
            case "postgres":
                $query = "SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '$TableName' and table_catalog=current_database()";
                foreach ($this->pdo->query($query) as $row)
                    $result[] = $row['column_name'];
                break;
        }
        return $result;
    }

    function GetTables() {
        $result = array();
        switch ($this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME)) {
            case "sqlite":
                $query = "SELECT name FROM sqlite_master WHERE type='table';";
                foreach ($this->pdo->query($query) as $row)
                    $result[] = $row['name'];
                break;
            case "mysql":
                $query = "select table_name from information_schema.tables where TABLE_SCHEMA=DATABASE();";
                foreach ($this->pdo->query($query) as $row)
                    $result[] = $row['table_name'];
                break;
            case "postgres":
                $query = "SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';";
                foreach ($this->pdo->query($query) as $row)
                    $result[] = $row['tablename'];
                break;
        }
        return $result;
    }

}
