<?php

namespace MyNamespace;

/**
 * Description of MyClass
 *
 * @author rainer
 */
class MyClass {
    public function __construct() {
        echo "Generating class " . __CLASS__ . ".\r\n";
        #strpos();
        #echo "5/0=" . (5/0);
    }
    
    public function func1()
    {
        echo __FUNCTION__ . PHP_EOL;
    }
    
    public function __invoke() {
        echo "Functor\r\n";
    }
}
