<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Algorithms\Graph\SpanningTree;

/**
 * Description of KruskalMinimalSpanningTree
 *
 * @author rainer
 */
class KruskalMinimalSpanningTree {
    private $DistanceMatrix = null;
    public static function newInstance() { return new static(); }
    public function setDistanceMatrix($DistanceMatrix) { $this->DistanceMatrix = $DistanceMatrix; return $this; }
    public function getDistanceMatrix() { return $this->DistanceMatrix; }
    
    public function Calculate()
    {
        
    }
}
