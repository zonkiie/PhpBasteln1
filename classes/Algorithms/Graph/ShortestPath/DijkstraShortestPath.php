<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Algorithms\Graph\ShortestPath;

use Utils\ArrayUtils;

/**
 * Description of DijkstraShortestPath
 *
 * @author rainer
 */
class DijkstraShortestPath extends AbstractShortestPath {

    public function __construct($DistanceMatrix = null) {
        $this->DistanceMatrix = $DistanceMatrix;
    }

    private function Init(&$Distance, &$PreviousNode, &$Q) {
        foreach ($this->DistanceMatrix as $Knot => $Edge) {
            $Distance[$Knot] = PHP_INT_MAX;
            $PreviousNode[$Knot] = null;
        }
        $Distance[$this->StartingPoint] = 0;
        $Q = array_keys($this->DistanceMatrix);
    }

    private function DistanceUpdate($u, $v, &$Distance, &$PreviousNode) {
        $Alternative = $Distance[$u] + $this->DistanceMatrix[$u][$v];
        if ($Alternative < $Distance[$v]) {
            $Distance[$v] = $Alternative;
            $PreviousNode[$v] = $u;
        }
    }
    
    private function Min_Dist($Distance, $Q)
    {
        $dist_value = PHP_INT_MAX;
        $dist_key = null;
        foreach($Q as $Value)
        {
            if($Distance[$Value]< $dist_value)
            {
                $dist_value = $Distance[$Value];
                $dist_key = $Value;
            }
        }
        return $dist_key;
    }
    
    /*
     * 
     */
    private function Dijkstra()
    {
        $Distance = array();
        $PreviousNode = array();
        $Q = array();
        $this->Init($Distance, $PreviousNode, $Q);
        while(count($Q) > 0)
        {
            $u = $this->Min_Dist($Distance, $Q);
            if($u === null) break;
            unset($Q[$u]);
            foreach($Q as $v)
            {
                if($v == $u) continue;
                $this->DistanceUpdate($u, $v, $Distance, $PreviousNode);
            }
        }
        return $PreviousNode;
    }
    
    /*
     * 
     */
    
    private function CreateShortestPath($PreviousNode)
    {
        $Path = array($this->TargetPoint);
        $u = $this->TargetPoint;
        while(($v = $PreviousNode[$u]) !== null)
        {
            $u = $PreviousNode[$u];
            array_unshift($Path, $u);
        }
        return $Path;
    }

    public function FindShortestPath() {
        $PreviousNode = $this->Dijkstra();
        $ShortestPath = $this->CreateShortestPath($PreviousNode);
        return $ShortestPath;
    }

}
