<?php

/* 
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


$eh = null;

set_exception_handler($eh = function(Exception $exception){ die("Exception:" . $exception->getMessage() . PHP_EOL . $exception->getTraceAsString() . PHP_EOL); });

set_error_handler(function($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) return;
    if(!in_array($severity, array(E_WARNING, E_NOTICE, E_CORE_WARNING, E_COMPILE_WARNING, E_USER_WARNING, E_USER_NOTICE, E_DEPRECATED, E_USER_DEPRECATED, E_STRICT))) throw new ErrorException($message, 0, $severity, $file, $line);
});

register_shutdown_function(function() {
    global $eh;
    $error = error_get_last();
    if($error != null && $error['type'] === E_ERROR) {
        #die("shutdown_function Fehler:" . print_r($error, true) . PHP_EOL);
        $eh(new Exception($error['message'] . ", File:" . $error['file'] . ", Line:" . $error['line']));
    }
});

