<?php

/* 
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

spl_autoload_register(
        function($ClassName)
        {
            $ClassPath = __DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $ClassName) . ".php";
            if(file_exists($ClassPath))
            {
                include_once ($ClassPath);
                if(class_exists($ClassName) || interface_exists($ClassName) || trait_exists($ClassName)) return true;
                else
                {
                    $ErrorMessage = "Klasse/Interface/Trait " . $ClassName . " konnte nicht geladen werden: Datei " . $ClassPath . " gefunden, aber Klasse/Interface/Trait nicht.";
                    die($ErrorMessage);
                }
            }
            else return false;
        }
);
