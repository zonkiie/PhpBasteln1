<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

/**
 * Description of ObjectFeeder
 *
 * @author rainer
 */
class ObjectFeeder {
    //put your code here
    private $sourceObject;
    private $destinationObject;
    private $ignoreFields = array();
    private $ignorePattern = null;
    
    public static function newInstance()
    {
        return new static;
    }
    
    public function setSourceObject(&$sourceObject)
    {
        $this->sourceObject = $sourceObject;
        return $this;
    }
    
    public function getSourceObject()
    {
        return $this->sourceObject;
    }
    
    public function setDestinationObject(&$destinationObject)
    {
        $this->destinationObject = $destinationObject;
        return $this;
    }
    
    public function getDestinationObject()
    {
        return $this->destinationObject;
    }
    
    public function setIgnoreFields(array $ignoreFields)
    {
        $this->ignoreFields = $ignoreFields;
        return $this;
    }
    
    public function getIgnoreFields()
    {
        return $this->ignoreFields;
    }
    
    public function setIgnorePattern($ignorePattern)
    {
        $this->ignorePattern = $ignorePattern;
        return $this;
    }
    
    public function getIgnorePattern()
    {
        return $this->ignorePattern;
    }
    
    public function SetKeyOrFieldValue($key, &$value)
    {
        if(is_array($this->destinationObject))
        {
            $this->destinationObject[$key] = $value;
        }
        else if(is_object($this->destinationObject))
        {
            $this->destinationObject->{$key} = $value;
        }
    }
    
    public function feed()
    {
        $fields = array();
        foreach($this->sourceObject as $key=>&$value)
        {
            if(in_array($key, $this->ignoreFields)) continue;
            if(!empty($this->ignorePattern) && preg_match($this->ignorePattern, $key)) continue;
            $this->SetKeyOrFieldValue($key, $value);
        }
    }
    
}
