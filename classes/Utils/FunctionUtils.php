<?php

/*
 * Copyright (C) 2017 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

/**
 * Description of FunctionUtils
 *
 * @author rainer
 */
class FunctionUtils {
    /**
     * Calls a method, function or closure. Parameters are supplied by their names instead of their position.
     * @param $call_arg like $callback in call_user_func_array()
     * Case1: {object, method}
     * Case2: {class, function}
     * Case3: "class::function"
     * Case4: "function"
     * Case5: closure
     * @param array $param_array A key-value array with the parameters
     * @return result of the method, function or closure
     * @throws \Exception when wrong arguments are given or required parameters are not given.
     */
    public static function call_user_function_named_param($call_arg, array $param_array)
    {
        $Func = null;
        $Method = null;
        $Object = null;
        $Class = null;
        // The cases. f means function name
        // Case1: f({object, method}, params)
        // Case2: f({class, function}, params)
        if(is_array($call_arg) && count($call_arg) == 2)
        {
            if(is_object($call_arg[0]))
            {
                $Object = $call_arg[0];
                $Class = get_class($Object);
            }
            else if(is_string($call_arg[0]))
            {
                $Class = $call_arg[0]; 
            }
            if(is_string($call_arg[1]))
            {
                $Method = $call_arg[1];
            }
        }
        // Case3: f("class::function", params)
        else if(is_string($call_arg) && strpos($call_arg, "::") !== FALSE)
        {
            list($Class, $Method) = explode("::", $call_arg);
        }
        // Case4: f("function", params)
        else if(is_string($call_arg) && strpos($call_arg, "::") === FALSE)
        {
            $Method = $call_arg;
        }
        // Case5: f(closure, params)
        else if(is_object($call_arg) && $call_arg instanceof \Closure)
        {
            $Method = $call_arg;
        }
        else throw new \Exception("Case not allowed! Invalid Data supplied!");
        if($Class) $Func = new \ReflectionMethod($Class, $Method);
        else $Func = new \ReflectionFunction($Method);
        $params = array();
        foreach($Func->getParameters() as $Param)
        {
            if($Param->isDefaultValueAvailable()) $params[$Param->getPosition()] = $Param->getDefaultValue();
            if(array_key_exists($Param->name, $param_array)) $params[$Param->getPosition()] = $param_array[$Param->name];
            if(!$Param->isOptional() && !isset($params[$Param->getPosition()])) die("No Defaultvalue available and no Value supplied!\r\n");
        }
        if($Func instanceof \ReflectionFunction) return $Func->invokeArgs($params);
        if($Func->isStatic()) return $Func->invokeArgs(null, $params);
        else return $Func->invokeArgs($Object, $params);
    }
}
