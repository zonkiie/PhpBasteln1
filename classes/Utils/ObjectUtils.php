<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

/**
 * Description of ObjectUtils
 *
 * @author rainer
 */
class ObjectUtils {
    
    public static function UnsetValueRecursive(&$data, $key)
    {
        if(is_array($data))
        {
            if(array_key_exists($key, $data)) unset($data[$key]);
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::UnsetValueRecursive ($inner_data, $key);
        }
        else if(is_object($data))
        {
            if(property_exists($data, $key)) unset($data->{$key});
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::UnsetValueRecursive ($inner_data, $key);
        }
    }
    
    public static function SetValueRecursive(&$data, $key, $value)
    {
        if(is_array($data))
        {
            if(array_key_exists($key, $data)) $data[$key] = $value;
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::SetValueRecursive ($inner_data, $key, $value);
        }
        else if(is_object($data))
        {
            if(property_exists($data, $key)) $data->{$key} = $value;
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::SetValueRecursive ($inner_data, $key, $value);
        }
    }
    
    public static function SetValueByAccessModifier(&$data, $modifier, $value = null)
    {
        if(is_array($data))
        {
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::SetValueByAccessModifier ($inner_data, $modifier, $value);
        }
        else if(is_object($data))
        {
            $RClazz = new \ReflectionClass($data);
            $Properties = $RClazz->getProperties($modifier);
            foreach($Properties as $Property)
            {
                $Property->setAccessible(TRUE);
                $Property->setValue($data, $value);
                $Property->setAccessible(FALSE);
            }
            
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::SetValueByAccessModifier ($inner_data, $modifier, $value);
        }
        
    }
    
    public static function SetValueByAnnotation(&$data, $annotation, $value = null)
    {
        $pattern = preg_quote($annotation) . '\b';
        if(is_array($data))
        {
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::SetValueByAnnotation ($inner_data, $annotation, $value);
        }
        else if(is_object($data))
        {
            $RClazz = new \ReflectionClass($data);
            $Properties = $RClazz->getProperties();
            foreach($Properties as $Property)
            {
                $docblock = $Property->getDocComment();
                if(!preg_match_all('|' . $pattern . '|i', $docblock)) continue;
                $Property->setAccessible(TRUE);
                $Property->setValue($data, $value);
                $Property->setAccessible(FALSE);
            }
            foreach($data as &$inner_data) if(is_array($inner_data) || is_object($inner_data)) static::SetValueByAnnotation ($inner_data, $annotation, $value);
        }
    }   
}
 
