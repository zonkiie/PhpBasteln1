<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

/**
 * Description of UnitUtils
 *
 * @author rainer
 */
class UnitUtils {
    const UnitPrefixes = "kMGTPEZY";
    const Base = 1024;
    //private $UnitPrefixes = array(1=>"k", 2=>"M", 3=>"G", 4=>"T", 5=>"P", 6=>"E", 7=>"Z", 8=>"Y");
    public static function CalculateToUnitPrefix($value)
    {
        $potf = log10((int)$value)/log10(static::Base);
        $poti = (int)$potf;
        $bvalue = pow(static::Base, ($potf - $poti));
        $prefix = substr(static::UnitPrefixes, $poti - 1, 1);
        return array("Value"=>$bvalue, "UnitPrefix"=>$prefix);
    }
    
    public static function CalculateFromUnitPrefix($numeric, $unit)
    {
        $index = strpos(static::UnitPrefixes, $unit);
        if($index === FALSE) return false;
        $index++;
        $retval = pow(static::Base, $index) * $numeric;
        return $retval;
    }
    
}
