<?php

/*
 * Copyright (C) 2017 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

/**
 * Description of FilesystemFunctions
 * File System Utils like find device for file path
 * @author rainer
 */
class FilesystemFunctions {
    public static function FindMountPoint($path)
    {
	$last_stat = array();
        $curr_stat = array();
	$dir = "";
        $old_dir = "";
	// Root Path is not mounted.
	if(!strcmp($path, "/")) return NULL;
	$dir = $path;
	do
	{
		$last_stat = stat($dir);
		$old_dir = realpath($dir);
		$dir = $old_dir;
		$dir .= "/../";
		$curr_stat = stat($dir);
		if($last_stat['dev'] != $curr_stat['dev'] || !strcmp($old_dir, "/")) return $old_dir;
		$old_dir = NULL;
	} while(true);
	return NULL;
    }
    
    public static function devname_from_procpartitions($major, $minor)
    {
            $procfilename = "/proc/partitions";
            $content = NULL;
            if(!file_exists($procfilename)) return NULL;
            $file = fopen($procfilename, "r");
            if($file == NULL) return NULL;
            $linecount = 0;
            while(($content = fgets($file)) != NULL)
            {
                    // now lets skip the first two lines of the file
                    $linecount++;
                    if($linecount < 2) continue;
                    $pmajor = -1;
                    $pminor = -1;
                    $blocks = "";
                    $name = "";
                    sscanf($content, "%d %d %lu %s", $pmajor, $pminor, $blocks, $name);
                    if($pmajor == $major && $pminor == $minor)
                    {
                            fclose($file);
                            return $name;
                    }
            }
            fclose($file);
            return NULL;
    }
    
    /**
     * @see https://www.mnxsolutions.com/golang/golang-determine-a-device-majorminor-number.html
     * @param type $path
     */
    public static function getdev($path)
    {
	$major = -1;
        $minor = -1;
	$st = array();
	if(!($st = stat($path))) return NULL;
	$major = (int)($st['dev']/256);
	$minor = (int)($st['dev']%256);
	$devname = self::devname_from_procpartitions($major, $minor);
	$result = "/dev/" . $devname;
	return $result;
        
    }

}
