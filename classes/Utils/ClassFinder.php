<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

use \Exception;
use \Reflection;
use \ReflectionClass;

/**
 * Description of ObjectFinder
 *
 * @author rainer
 * @see http://stackoverflow.com/questions/928928/determining-what-classes-are-defined-in-a-php-class-file
 */
class ClassFinder {
    public static function newInstance() { return new static; }
    private $BaseDir = null;
    private $Recursive = FALSE;
    
    public function SetBaseDir($BaseDir)
    {
        $this->BaseDir = $BaseDir;
        return $this;
    }
    
    public function SetRecursive($Recursive)
    {
        $this->Recursive = $Recursive;
        return $this;
    }
    
    /**
     * 
     * @param String $Namespace The Namespace
     * @param Boolean $recursive True if all sub-namespaces should be scanned
     * @return array List of all Classes in given Namespace
     * Convention: All namespaces correspond to directory names
     */
    public function FindAllClassesInNamespace($Namespace)
    {
        $dirpart = str_replace('\\', '/', $Namespace);
        $startdir = realpath($this->BaseDir . DIRECTORY_SEPARATOR . $dirpart);
        $classes = array();
        $this->FindAllClassesInDir($classes, $startdir);
        return $classes;
    }
    
    private function JumpToNonWSToken(&$Token_array)
    {
        $this->JumpToNonToken($Token_array, T_WHITESPACE);
    }
    
    private function JumpToNonToken(&$Token_array, $Token_class)
    {
        $token = current($Token_array);
        do { $token = next($Token_array); } while($token !== FALSE && $token[0] == $Token_class);
    }
    
    private function JumpToToken(&$Token_array, $Token_class)
    {
        $token = current($Token_array);
        do { $token = next($Token_array); } while($token !== FALSE && $token[0] != $Token_class);
    }
    
    public function GetClassesInFile($Path)
    {
        if(!file_exists($Path)) throw new Exception("File " . $Path . " not found!");
        $contents = file_get_contents($Path);
        if(empty($contents)) return null;
        $classes = array();
        $tokens = token_get_all($contents);
        $namespace = null;
        // Process Tokens here
        
        $token = reset($tokens);
        do {
            if(count($token) > 0 && $token[0] === T_NAMESPACE)
            {
                $this->JumpToNonWSToken($tokens);
                $nexttoken = current($tokens);
               if($nexttoken !== FALSE) $namespace = $nexttoken[1];
            }
            if(count($token) > 0 && $token[0] === T_CLASS)
            {
                $this->JumpToNonWSToken($tokens);
                $nexttoken = current($tokens);
                array_push($classes, $namespace . '\\' . $nexttoken[1]);
            }
        } while($token = next($tokens));
        
        return $classes;
    }
    
    public function FindAllClassesInDir(&$Result, $Dir = ".")
    {
        if(is_null($Result)) $Result = array();
        $entries = scandir($Dir);
        foreach($entries as $entry)
        {
            if($entry === "." || $entry === "..") continue;
            $fp = $Dir . DIRECTORY_SEPARATOR . $entry;
            if(is_dir($fp) && $this->Recursive) $this->FindAllClassesInDir ($Result, $fp);
            else if(is_file($fp))
            {
                $file_result = $this->GetClassesInFile($fp);
                if(!empty($file_result)) $Result = array_merge($Result, $this->GetClassesInFile($fp));
            }
        }
    }
    
    public function FindAllClassesImplementingInterface($Interface, $Namespace = "")
    {
        $result = array();
        $classes = $this->FindAllClassesInNamespace($Namespace);
        foreach($classes as $class)
        {
            $ReflClass = new \ReflectionClass($class);
            if($ReflClass->implementsInterface($Interface)) $result[] = $class;
        }
        return $result;
    }
    
    public function FindAllClassesExtendBaseClass($BaseClass, $Namespace = "")
    {
        $result = array();
        $classes = $this->FindAllClassesInNamespace($Namespace);
        foreach($classes as $class)
        {
            $ReflClass = new \ReflectionClass($class);
            if($ReflClass->isSubclassOf($BaseClass)) $result[] = $class;
        }
        return $result;
    }
    
}
