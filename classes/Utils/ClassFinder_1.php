<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

use \Exception;
use \Reflection;
use \ReflectionClass;

/**
 * Description of ObjectFinder
 *
 * @author rainer
 * @see http://stackoverflow.com/questions/928928/determining-what-classes-are-defined-in-a-php-class-file
 */
class ClassFinder_1 {
    public static function newInstance() { return new static; }
    private $BaseDir = null;
    
    public function SetBaseDir($BaseDir)
    {
        $this->BaseDir = $BaseDir;
        return $this;
    }
    /**
     * 
     * @param String $namespace The Namespace
     * @param Boolean $recursive True if all sub-namespaces should be scanned
     * @return array List of all Classes in given Namespace
     * Convention: All namespaces correspond to directory names
     */
    public function FindAllClassesInNamespace($namespace, $recursive = FALSE)
    {
        $dirpart = str_replace('\\', '/', $namespace);
        $startdir = realpath($this->BaseDir . DIRECTORY_SEPARATOR . $dirpart);
        $classes = array();
        $this->FindAllClassesInDir($classes, $startdir, $recursive);
        return $classes;
    }
    
    private function JumpToNonWSToken(&$token_array)
    {
        $this->JumpToNonToken($token_array, T_WHITESPACE);
    }
    
    private function JumpToNonToken(&$token_array, $token_class)
    {
        $token = current($token_array);
        do { $token = next($token_array); } while($token !== FALSE && $token[0] == $token_class);
    }
    
    private function JumpToToken(&$token_array, $token_class)
    {
        $token = current($token_array);
        do { $token = next($token_array); } while($token !== FALSE && $token[0] != $token_class);
    }
    
    public function GetClassesInFile($path)
    {
        if(!file_exists($path)) throw new Exception("File " . $path . " not found!");
        $contents = file_get_contents($path);
        if(empty($contents)) return null;
        $classes = array();
        $tokens = token_get_all($contents);
        $namespace = null;
        // Process Tokens here
        
        $token = reset($tokens);
        do {
            if(count($token) > 0 && $token[0] === T_NAMESPACE)
            {
                $this->JumpToNonWSToken($tokens);
                $nexttoken = current($tokens);
               if($nexttoken !== FALSE) $namespace = $nexttoken[1];
            }
            if(count($token) > 0 && $token[0] === T_CLASS)
            {
                $this->JumpToNonWSToken($tokens);
                $nexttoken = current($tokens);
                array_push($classes, $namespace . '\\' . $nexttoken[1]);
            }
        } while($token = next($tokens));
        
        return $classes;
    }
    
    public function FindAllClassesInDir(&$result, $dir = ".", $recursive = FALSE)
    {
        if(is_null($result)) $result = array();
        $entries = scandir($dir);
        foreach($entries as $entry)
        {
            if($entry === "." || $entry === "..") continue;
            $fp = $dir . DIRECTORY_SEPARATOR . $entry;
            if(is_dir($fp) && $recursive) $this->FindAllClassesInDir ($result, $fp, $recursive);
            else if(is_file($fp))
            {
                $file_result = $this->GetClassesInFile($fp);
                if(!empty($file_result)) $result = array_merge($result, $this->GetClassesInFile($fp));
            }
        }
    }
    
    public function FindAllClassesImplementingInterface($Interface, $Namespace = "", $recursive = FALSE)
    {
        $result = array();
        $classes = $this->FindAllClassesInNamespace($Namespace, $recursive);
        foreach($classes as $class)
        {
            $ReflClass = new \ReflectionClass($class);
            if($ReflClass->implementsInterface($Interface)) $result[] = $class;
        }
        return $result;
    }
    
    public function FindAllClassesExtendBaseClass($BaseClass, $Namespace = "", $recursive = FALSE)
    {
        $result = array();
        $classes = $this->FindAllClassesInNamespace($Namespace, $recursive);
        foreach($classes as $class)
        {
            $ReflClass = new \ReflectionClass($class);
            if($ReflClass->isSubclassOf($BaseClass)) $result[] = $class;
        }
        return $result;
    }
    
}
