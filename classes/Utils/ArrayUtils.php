<?php

/*
 * Copyright (C) 2016 rainer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Utils;

/**
 * Description of ArrayUtils
 *
 * @author rainer
 */
class ArrayUtils {
    /*
     * finds the key with the minimum value in $array
     */
    public static function array_min_key($array)
    {
        $min_key = null;
        $min_value = null;
        foreach($array as $key=>$value)
        {
            if($min_value && $value < $min_value){
                $min_value = $value;
                $min_key = $key;
            }
            else {
                $min_value = $value;
                $min_key = $key;
            }
        }
        return $min_key;
    }
    
    public static function array_max_key($array)
    {
        $max_key = null;
        $max_value = null;
        foreach($array as $key=>$value)
        {
            if($max_value && $value > $max_value){
                $max_value = $value;
                $max_key = $key;
            }
            else {
                $max_value = $value;
                $max_key = $key;
            }
        }
        return $max_key;
    }

    public static function array_keep_shift(array &$new_array, array $input, bool $keep_key = false)
    {
        $new_array = [];
        $removed_element = null;
        if(is_array($input) && count($input) > 0)
        {
            foreach($input as $ekey=>$element)
            {
                if($removed_element == null) $removed_element = $element;
                else
                {
                    if($keep_key == false) $new_array[] = $element;
                    else $new_array[$ekey] = $element;
                }
            }
        }
        return $removed_element;
    }

    public static function get_array_recursive(array $structure, array $items)
    {
        $items2 = [];
        $element = self::array_keep_shift($items2, $items);
        if(count($items) > 1)
        {
            return self::get_array_recursive($structure[$element], $items2);
        }
        else
        {
            return $structure[$element];
        }
    }

    public static function sort_by_keys(&$products, $sortkeys = null)
    {
        uasort($products, function($item1, $item2) use($sortkeys) {
            return self::get_array_recursive($item1, $sortkeys) <=> self::get_array_recursive($item2, $sortkeys);
        });
    }

}
